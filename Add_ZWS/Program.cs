﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;

namespace Add_ZWS
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.InputEncoding = Encoding.Unicode;
            Console.OutputEncoding = Encoding.Unicode;

            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            while (true)
            {
                Console.WriteLine("Copy & Paste full path to the mqxlz file");
                string @file = Console.ReadLine();
                file = file.Replace("\"", "");

                string path = new FileInfo(file).DirectoryName;
                string filename = new FileInfo(file).Name;
                string tempdir = "Temp";
                string xliff = "document.mqxliff";

                Console.WriteLine($"{path}/{filename}");

                Decompress(path, filename, tempdir);
                ProcessFile(path, xliff, tempdir, file);
                Console.WriteLine("Done\n\nMay I have another?");
            }



        }

        static void Decompress(string path, string filename, string tempdir)
        {
            if (Directory.Exists(Path.Combine(path, tempdir)) == true)
            {
                Directory.Delete(Path.Combine(path, tempdir), true);
                Directory.CreateDirectory(Path.Combine(path, tempdir));
                ZipFile.ExtractToDirectory(Path.Combine(path, filename), Path.Combine(path, tempdir));
            }
            else
            {
                Directory.CreateDirectory(Path.Combine(path, tempdir));
                ZipFile.ExtractToDirectory(Path.Combine(path, filename), Path.Combine(path, tempdir));
            }

        }

        static void ProcessFile(string path, string filename, string tempdir, string originalfile)
        {
            //create an Excel for the "Damage" report
            ExcelPackage report = new ExcelPackage();
            ExcelWorksheet worksheet = report.Workbook.Worksheets.Add("Sheet1");

            //Load file
            XmlDocument xliff = new XmlDocument();
            string mqxliff = Path.Combine(path, tempdir, filename);
            //Console.WriteLine(mqxliff);
            xliff.Load(mqxliff);

            XmlNodeList trans_units = xliff.GetElementsByTagName("trans-unit");
            List<TransUnit> tu = new List<TransUnit>();

            int rown = 1;
            int memoq_row = 1;
            worksheet.Cells[rown, 1].Value = "row";
            worksheet.Cells[rown, 2].Value = "ID";
            worksheet.Cells[rown, 3].Value = "Source";
            worksheet.Cells[rown, 4].Value = "Original Target";
            //worksheet.Cells[rown, 5].Value = "Kanji Corrected";
            //worksheet.Cells[rown, 6].Value = "Over 15 Chars";
            worksheet.Cells[rown, 5].Value = "Final";
            rown++;

            for (int i = 0; i < trans_units.Count; i++)
            {
                string context = "";
                string src = "";
                string trg = ""; //this is what we will actually be working with
                StringBuilder before_zws = new StringBuilder();
                StringBuilder after_zws = new StringBuilder();



                for (int j = 0; j < trans_units[i].ChildNodes.Count; j++)
                {
                    if (trans_units[i].ChildNodes[j].LocalName == "source")
                    {

                        src = trans_units[i].ChildNodes[j].InnerText;
                        worksheet.Cells[rown, 1].Value = memoq_row;
                        worksheet.Cells[rown, 3].Value = src;
                        memoq_row++;
                    }





                    if (trans_units[i].ChildNodes[j].LocalName == "target")
                    {

                        trg = trans_units[i].ChildNodes[j].InnerXml;

                        for (int x = 0; x < trans_units[i].ChildNodes[j].ChildNodes.Count; x++)
                        {
                            string sentence = trans_units[i].ChildNodes[j].ChildNodes[x].Value;
                            string sentence_visual = "";
                            string zwspattern = @"\u200B";
                            string nokanjikanjipattern = @"([\P{IsCJKUnifiedIdeographs}])([\p{IsCJKUnifiedIdeographs}])";
                            string zwsbylengthpattern = @"((.){15})(?!$)";

                            //clean segments
                            if (sentence != null)
                            {
                                string visual = sentence;
                                Match m = Regex.Match(sentence, zwspattern, RegexOptions.IgnoreCase);
                                if (m.Success)
                                {
                                    sentence = Regex.Replace(sentence, zwspattern, "");
                                }

                                Match v = Regex.Match(visual, zwspattern, RegexOptions.IgnoreCase);
                                if (v.Success)
                                {
                                    visual = Regex.Replace(visual, zwspattern, "[Ben]");
                                }
                                //Console.WriteLine($"{i}::{j}::{x}::{visual}");
                                //Construct the segment with the [ZWS]
                                //worksheet.Cells[rown, 4].Value = visual.Replace("[Ben]", "[ZWS]");
                                before_zws.Append(visual.Replace("[Ben]", "[ZWS]"));

                            }
                            else
                            {
                                before_zws.Append("\n");
                            }

                            //add ZWS before a Kanji that is not preceded by another Kanji
                            if (sentence != null)
                            {
                                string visual = sentence;

                                Match m = Regex.Match(sentence, nokanjikanjipattern, RegexOptions.IgnoreCase);
                                if (m.Success)
                                {
                                    sentence = Regex.Replace(sentence, nokanjikanjipattern, "$1\u200B$2");
                                }

                                Match v = Regex.Match(visual, nokanjikanjipattern, RegexOptions.IgnoreCase);
                                if (v.Success)
                                {
                                    visual = Regex.Replace(visual, nokanjikanjipattern, "$1[Rob]$2");
                                }

                                //Console.WriteLine($"{i}::{j}::{x}::{visual}");
                                //worksheet.Cells[rown, 5].Value = visual;
                            }

                            //add ZWS at least every 15 characters (including kana, kanji and punctuation
                            if (sentence != null)
                            {
                                string[] words = sentence.Split('\u200B');

                                //Console.WriteLine($"subsegments found: {words.Length}");
                                StringBuilder subsentence = new StringBuilder();
                                for (int wx = 0; wx < words.Length; wx++)
                                {
                                    //Console.WriteLine($"characters: {words[wx].Length} => {words[wx]}");

                                    string visual = words[wx];

                                    //MatchCollection jpwords = Regex.Matches(words[wx], @"[\p{IsCJKUnifiedIdeographs}\p{IsHiragana}\p{IsKatakana}\u200B]+");

                                    ///MatchCollection enwords = Regex.Matches(words[wx], @"[\p{IsBasicLatin}\u200B]+");
                                    //アクロポリス・ZWSラリーでのLancia ZWSDelta ZWSHF ZWS4WD
                                    /*
                                    if (jpwords.Count > 0)
                                    {

                                        for (int jgrp = 0; jgrp < jpwords.Count; jgrp++)
                                        {
                                            Console.WriteLine($"before: {wx}::{jpwords[jgrp].Value}");


                                            Match m = Regex.Match(jpwords[jgrp].Value, zwsbylengthpattern, RegexOptions.IgnoreCase);
                                            if (m.Success)
                                            {
                                                words[wx] = Regex.Replace(jpwords[jgrp].Value, zwsbylengthpattern, "$1\u200B");
                                            }

                                            Match v = Regex.Match(jpwords[jgrp].Value, zwsbylengthpattern, RegexOptions.IgnoreCase);
                                            if (v.Success)
                                            {
                                                visual = Regex.Replace(jpwords[jgrp].Value, zwsbylengthpattern, "$1[Dave]");
                                            }

                                            

                                        }
                                    }
                                    */
                                    /*

                                    Console.WriteLine($"after: {wx}::{visual}");

                                    if (enwords.Count > 0)
                                    {

                                        for (int egrp = 0; egrp < enwords.Count; egrp++)
                                        {
                                            Console.WriteLine($"before: {wx}::{enwords[egrp].Value}");


                                            Match m = Regex.Match(enwords[egrp].Value, "( )", RegexOptions.IgnoreCase);
                                            if (m.Success)
                                            {
                                                words[wx] = Regex.Replace(enwords[egrp].Value, "( )", "$1\u200B");
                                            }

                                            Match v = Regex.Match(enwords[egrp].Value, "( )", RegexOptions.IgnoreCase);
                                            if (v.Success)
                                            {
                                                visual = Regex.Replace(enwords[egrp].Value, "( )", "$1[Dave]");
                                            }



                                        }
                                    }
                                    */




                                    Match m = Regex.Match(words[wx], zwsbylengthpattern, RegexOptions.IgnoreCase);
                                    if (m.Success)
                                    {
                                        words[wx] = Regex.Replace(words[wx], zwsbylengthpattern, "$1\u200B");
                                    }

                                    Match v = Regex.Match(visual, zwsbylengthpattern, RegexOptions.IgnoreCase);
                                    if (v.Success)
                                    {
                                        visual = Regex.Replace(visual, zwsbylengthpattern, "$1[Dave]");
                                    }





                                    //Console.WriteLine($"wx:{wx}::{i}::{j}::{x}::{visual}");
                                }

                                StringBuilder reassemble = new StringBuilder();
                                StringBuilder reassemble_visual = new StringBuilder();
                                for (int wx = 0; wx < words.Length; wx++)
                                {
                                    if (wx != words.Length - 1)
                                    {
                                        reassemble.Append($"{words[wx]}\u200B");
                                        reassemble_visual.Append($"{words[wx].Replace("\u200B", "[Dave]")}[Rob]");
                                    }
                                    else
                                    {
                                        //the last item in the sentence should not end with a ZWS
                                        reassemble.Append($"{words[wx]}");
                                        reassemble_visual.Append($"{words[wx].Replace("\u200B", "[Dave]")}");
                                    }
                                }

                                sentence = reassemble.ToString();
                                sentence_visual = reassemble_visual.ToString();
                                //worksheet.Cells[rown, 6].Value = sentence_visual;
                                //Console.WriteLine("");
                            }

                            //fix english
                            if (sentence != null)
                            {


                                Match enzws = Regex.Match(sentence, @"([\p{IsBasicLatin}])\u200B");
                                if (enzws.Success)
                                {
                                    sentence = Regex.Replace(sentence, @"([\p{IsBasicLatin}])\u200B", "$1");
                                }

                                //fix space
                                Match en_space = Regex.Match(sentence, @"([\p{IsBasicLatin}]) ");
                                if (en_space.Success)
                                {
                                    sentence = Regex.Replace(sentence, @"([\p{IsBasicLatin}]) ", "$1 \u200B");
                                }
                                //fix space if zws is before the space
                                Match en_space_zws = Regex.Match(sentence, @"([\p{IsBasicLatin}])\u200B ");
                                if (en_space_zws.Success)
                                {
                                    sentence = Regex.Replace(sentence, @"([\p{IsBasicLatin}])\u200B ", "$1 \u200B");
                                }
                            }

                            if (sentence != null)
                            {
                                //Console.WriteLine($"Reassembled:             {sentence_visual}");
                                //Console.WriteLine($"Before Final (cleaning): {sentence.Replace("\u200B", "[Raph]")}");
                                //clean the final sentence of [ZWS] before a punctuation Full-Width and Half-Width
                                sentence = sentence.Replace("\u200B｛　", "｛　\u200B");
                                sentence = sentence.Replace("\u200B｝", "｝\u200B");
                                sentence = sentence.Replace("\u200B（　", "（　\u200B");
                                sentence = sentence.Replace("\u200B）", "）\u200B");
                                sentence = sentence.Replace("\u200B［　", "［　\u200B");
                                sentence = sentence.Replace("\u200B］", "］\u200B");
                                sentence = sentence.Replace("\u200B【", "】\u200B");
                                sentence = sentence.Replace("\u200B、", "、\u200B");
                                sentence = sentence.Replace("\u200B，", "，\u200B");
                                sentence = sentence.Replace("\u200B…", "…\u200B");
                                sentence = sentence.Replace("\u200B‥", "‥\u200B");
                                sentence = sentence.Replace("\u200B。", "。\u200B");
                                sentence = sentence.Replace("\u200B「", "「\u200B");
                                sentence = sentence.Replace("\u200B」", "」\u200B");
                                sentence = sentence.Replace("\u200B『", "『\u200B");
                                sentence = sentence.Replace("\u200B』", "』\u200B");
                                sentence = sentence.Replace("\u200B〝", "〝\u200B");
                                sentence = sentence.Replace("\u200B〟", "〟\u200B");
                                sentence = sentence.Replace("\u200B⟨", "⟨\u200B");
                                sentence = sentence.Replace("\u200B⟩", "⟩\u200B");
                                sentence = sentence.Replace("\u200B〜", "〜\u200B");
                                sentence = sentence.Replace("\u200B：", "：\u200B");
                                sentence = sentence.Replace("\u200B！", "！\u200B");
                                sentence = sentence.Replace("\u200B？", "？\u200B");
                                sentence = sentence.Replace("\u200B♪", "♪\u200B");

                                sentence = sentence.Replace("\u200B[", "[\u200B");
                                sentence = sentence.Replace("\u200B]", "]\u200B");
                                sentence = sentence.Replace("\u200B(", "(\u200B");
                                sentence = sentence.Replace("\u200B)", ")\u200B");
                                sentence = sentence.Replace("\u200B}", "}\u200B");
                                sentence = sentence.Replace("\u200B{", "{\u200B");
                                sentence = sentence.Replace("\u200B.", ".\u200B");
                                sentence = sentence.Replace("\u200B!", "!\u200B");
                                sentence = sentence.Replace("\u200B?", "?\u200B");
                                sentence = sentence.Replace("\u200B~", "~\u200B");
                                sentence = sentence.Replace("\u200B\"", "\"\u200B");
                                sentence = sentence.Replace("\u200B:", ":\u200B");
                                sentence = sentence.Replace("\u200B;", ";\u200B");
                                sentence = sentence.Replace("\u200B&", "&\u200B");
                                sentence = sentence.Replace("\u200B<", "<\u200B");
                                sentence = sentence.Replace("\u200B>", ">\u200B");

                                //we shouldn't have a 
                                sentence = Regex.Replace(sentence, "\u200B$", "");

                                //uncomment to test
                                //trans_units[i].ChildNodes[j].ChildNodes[x].InnerText = sentence.Replace("\u200B", "[ZWS]");

                                trans_units[i].ChildNodes[j].ChildNodes[x].InnerText = sentence;

                                //Console.WriteLine($"Final:                   {sentence.Replace("\u200B", "[ZWS]")}");
                                //worksheet.Cells[rown, 5].Value = sentence.Replace("\u200B", "[ZWS]");
                                after_zws.Append(sentence.Replace("\u200B", "[ZWS]"));
                                //rown++;
                            }
                            else
                            {
                                sentence = trans_units[i].ChildNodes[j].ChildNodes[x].InnerText;
                                trans_units[i].ChildNodes[j].ChildNodes[x].InnerText = sentence;
                                //Console.WriteLine(sentence);
                                after_zws.Append("\n");
                            }
                            //Console.WriteLine("");

                        }

                    }


                    if (trans_units[i].ChildNodes[j].LocalName == "context-group")
                    {

                        for (int ctx_grp = 0; ctx_grp < trans_units[i].ChildNodes[j].ChildNodes.Count; ctx_grp++)
                        {
                            for (int ctx_attr = 0; ctx_attr < trans_units[i].ChildNodes[j].ChildNodes[ctx_grp].Attributes.Count; ctx_attr++)
                            {

                                if (trans_units[i].ChildNodes[j].ChildNodes[ctx_grp].Attributes[ctx_attr].LocalName == "context-type" && trans_units[i].ChildNodes[j].ChildNodes[ctx_grp].Attributes[ctx_attr].Value == "x-mmq-context")
                                {
                                    context = trans_units[i].ChildNodes[j].ChildNodes[ctx_grp].InnerText;
                                    worksheet.Cells[rown, 2].Value = context;
                                    worksheet.Cells[rown, 4].Value = before_zws.ToString();
                                    worksheet.Cells[rown, 5].Value = after_zws.ToString();
                                    rown++;
                                }

                            }

                        }

                    }

                }

                //tu.Add(new TransUnit(i,context, src, trg));

            }

            //write out
            var settings = new XmlWriterSettings();
            settings.Indent = true;

            using (var writer = XmlWriter.Create(Path.Combine(path, tempdir, "temp.mqxliff"), settings))
            {
                xliff.Save(writer);
            }

            string all_text = File.ReadAllText(Path.Combine(path, tempdir, "temp.mqxliff"));

            all_text = Regex.Replace(all_text, "^([ ]+)(?=<)", "", RegexOptions.Multiline);

            File.Delete(Path.Combine(path, tempdir, "document.mqxliff"));
            File.WriteAllText(Path.Combine(path, tempdir, "document.mqxliff"), all_text);
            File.Delete(Path.Combine(path, tempdir, "temp.mqxliff"));

            if (File.Exists(originalfile.Replace(".mqxlz", "_finalized.mqxlz")))
            {
                File.Delete(originalfile.Replace(".mqxlz", "_finalized.mqxlz"));
            }
            //compress
            ZipFile.CreateFromDirectory(Path.Combine(path, tempdir), originalfile.Replace(".mqxlz", "_finalized.mqxlz"));

            //cleanup
            Directory.Delete(Path.Combine(path, tempdir), true);

            report.SaveAs(new FileInfo(Path.Combine(path, $"{originalfile.Replace(".mqxlz", "")}_report.xlsx")));


        }



    }

    class TransUnit
    {
        private int id;
        private string context;
        private string source;
        private string target;


        public TransUnit(int id, string context, string source, string target)
        {
            this.id = id;
            this.context = context;
            this.source = source;
            this.target = target;

        }

        public int ID
        {
            get { return id; }
        }
        public string Context
        {
            get { return context; }
        }
        public string Source
        {
            get { return source; }
        }

        public string Target
        {
            get { return target; }
        }
    }
}
